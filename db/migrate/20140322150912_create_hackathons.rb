class CreateHackathons < ActiveRecord::Migration
  def change
    create_table :hackathons do |t|
      t.string :title
      t.datetime :date
      t.string :name
      t.text :presentation
      t.text :goal

      t.timestamps
    end
  end
end
